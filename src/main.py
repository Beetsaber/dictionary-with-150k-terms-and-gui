import sys

TERM = ""
TERM_DEFINITION = ""

def main(argv):
	global TERM
	global TERM_DEFINITION
	TERM_DEFINITION = ""

	DB_NAME = "wordnet_2_0.db"
	
	try:
		f = open(DB_NAME, "r")
	except FileNotFoundError:
		print("File ("+DB_NAME+") Not Found Error")
		return
	
	
	lines = f.readlines()

	searchWord = ""
	if len(argv) < 1:
		pass
		#searchWord = input("Enter word to search:")
	else: searchWord = argv[0]
	TERM = searchWord

	i = 0
	while i < len(lines):
		
		if lines[i][0] != ' ':# line not starting with whitespace (ie. term)
			if lines[i].strip() == searchWord:
				print("->", lines[i].strip())# print term
				i += 1
				while lines[i][0] == ' ':# as long as there is white space as first char of line (ie. term definition)
					print(lines[i])# print definition of the term
					TERM_DEFINITION += lines[i]
					i += 1
				i -= 1# go to last line which holds term definition because next line will be automatically advanced
			
		i += 1# advance to next line
		
		
	f.close()
		
		
if __name__ == "__main__": main(sys.argv[1:])


import tkinter as tk

root = tk.Tk()
label = tk.Label(root, text=TERM, fg = "dark blue", font = "Times 16 bold italic")
label.pack()

data_string = tk.StringVar()
data_string.set(TERM_DEFINITION)
lineNum = data_string.get().count("\n")


text = tk.Text(root, fg="brown" , bg="light blue", font = "Helvetica 14", bd=2, width=56, height=lineNum+1)
text.insert(tk.INSERT, data_string.get())
text.pack()

input_string = tk.StringVar()
e = tk.Entry(root, textvariable=input_string, fg="black", bg="white", bd=0)
e.focus()
e.pack()


def enterPressed(event):
	main([input_string.get()])
	
	label.config(text=TERM)
	
	data_string.set(TERM_DEFINITION)
	text.delete(1.0, tk.END)
	lineNum = data_string.get().count("\n")+1
	text.config(height=lineNum)
	text.insert(tk.INSERT, data_string.get())
	
root.bind('<Return>', enterPressed)

root.mainloop()